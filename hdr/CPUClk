; Copyright 2011 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

        GET   Hdr:CPUClkDevice

; Some TPS registers
TPSPM_IIC           * &4B

DCDC_GLOBAL_CFG     * &61
VDD1_VSEL           * &B9
VDD1_VMODE_CFG      * &BA

; OPP table format
                 ^  0
OPPTbl_MHz       #  2 ; Max MHz value for this voltage
OPPTbl_VDD1      #  1 ; Required VDD1 value, in VDD1_VSEL format
OPPTbl_CLKOUT_M2 #  1 ; Required DPLL1CLKOUT_M2 value
OPPTbl_Size      #  0 ; Size of each entry

OPPTbl_Format    *  0 ; Format number as returned by CPUClk_Override

OPPTbl_Max       *  17 ; Max number of entries we support (chosen to make us same size as SR driver)

; CPU clock device
                   ^  0, a1
; Public bits
CPUClkDevice       #  HALDevice_CPUClk_Size_0_2 ; support API 0.2
; Private bits
CPUClkShutdown     #  4 ; Pointer to shutdown func. Must be at same offset as SR37xShutdown!
CPUClkWorkspace    #  4 ; HAL workspace pointer
CPUClkNewSpeed     #  4 ; Re-entrancy flag. -1 if idle, desired table idx if in process of changing CPU speed. Allows CPUClk_Get to finish changing the speed if it gets called in the middle of a change.
CPUClkCurSpeed     #  4 ; Current table idx
CPUClkOPPTblSize   #  4 ; Number of entries in table
CPUClkOPPTbl       #  OPPTbl_Size*OPPTbl_Max ; OPP table
CPUClkOPPDefault   #  OPPTbl_Size ; Default OPP settings, for shutdown
CPUClk_DeviceSize  *  :INDEX: @

CPUClk_WorkspaceSize  * CPUClk_DeviceSize

        END
